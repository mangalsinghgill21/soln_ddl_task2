﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Soln_DDL_Task2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Task 2</title>
    <link href="Css/css.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    
    </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                 <table class="w3-table-all" style="width:400px; margin-left: auto;  margin-right: auto; height:200px" >
                    
                     <tr>
                        <td style="font-size:20px">
                            <asp:Label ID="lblmessage" runat="server" Text="Select color to show" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddl" runat="server" CssClass="w3-dropdown-hover w3-button " OnSelectedIndexChanged="ddl_SelectedIndexChanged" AutoPostBack="true"  >
                                  <asp:ListItem Value="0">--Select--</asp:ListItem>
                                  <asp:ListItem Value="Red">Red</asp:ListItem>
                                <asp:ListItem Value="Green">Green</asp:ListItem>
                                <asp:ListItem Value="Blue">Blue</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    
                  
                </table><br />
                
                <div  id ="divcolor" class="square" runat="server">                 
                      Color Changes......
                  </div>
              </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
